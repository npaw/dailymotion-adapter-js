var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.Dailymotion = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    var adsAdapter = this.plugin.getAdsAdapter()
    if (!adsAdapter || !adsAdapter.flags.isStarted){
      this.lastPlayhead = this.player.currentTime
    }
    return this.lastPlayhead
  },

  /** Override to return current playrate */
  /* getPlayrate: function() {
    return null
  }, */

  /** Override to return video duration */
  getDuration: function () {
    return (isNaN(this.player.duration)) ? null : this.player.duration
  },

  /** Override to return rendition */
  getRendition: function () {
    return this.player.quality
  },

  /** Override to return title */
  getTitle: function () {
    return this.player.video.title
  },

  /** Override to recurn true if live and false if VOD */
  getIsLive: function () {
    return false
  },

  /** Override to return resource URL. */
  getResource: function () {
    return this.player.src
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'Dailymotion'
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Console all events if logLevel=DEBUG
    youbora.Util.logAllEvents(this.player, [null,
      'ad_end',
      'ad_pause',
      'ad_play',
      'ad_start',
      // 'ad_timeupdate',
      'apiready',
      'controlschange',
      'durationchange',
      'end',
      'ended',
      'error',
      'fullscreenchange',
      'loadedmetadata',
      'pause',
      'play',
      'playing',
      'progress',
      'qualitiesavailable',
      'seeked',
      'seeking',
      'subtitlechange',
      'subtitlesavailable',
      'start',
      // 'timeupdate',
      'video_start',
      'video_end'
    ])

    // Enable playhead monitor (buffer = true, seek = false)
    //if (!this.monitor) this.monitorPlayhead(true, false)

    // References
    this.references = {
      'start': this.startListener.bind(this),
      'play': this.playListener.bind(this),
      'pause': this.pauseListener.bind(this),
      'playing': this.playingListener.bind(this),
      'error': this.errorListener.bind(this),
      'seeking': this.seekingListener.bind(this),
      'seeked': this.seekedListener.bind(this),
      'ended': this.endedListener.bind(this),
      'apiready': this.registerAdsListener.bind(this),
      'timeupdate': this.timeupdateListener.bind(this),
      'waiting': this.bufferingListener.bind(this)
    }

    // Register listeners
    for (var key in this.references) {
      this.player.addEventListener(key, this.references[key])
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    //if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.removeEventListener(key, this.references[key])
      }
      delete this.references
    }
  },

  startListener: function (e) {
    this.fireStop()
    this.fireStart()
  },

  registerAdsListener: function (e) {
    this.plugin.setAdsAdapter(new youbora.adapters.Dailymotion.GenericAdsAdapter(this.player))
  },

  /** Listener for 'play' event. */
  playListener: function (e) {
    this.fireStart()
  },

  /** Listener for 'pause' event. */
  pauseListener: function (e) {
    this.firePause()
  },

  /** Listener for 'playing' event. */
  playingListener: function (e) {
    this.fireStart()
    this.fireResume()
    this.fireJoin()
    this.fireBufferEnd()
  },

  bufferingListener: function (e) {
    this.fireBufferBegin()
  },

  /** Listener for 'error' event. */
  errorListener: function (e) {
    var error = this.player.error
    if (error) {
      this.fireFatalError(error.code + ' ' + error.title, error.message)
    } else {
      this.fireError()
    }
  },

  /** Listener for 'seeking' event. */
  seekingListener: function (e) {
    this.fireSeekBegin()
  },

  /** Listener for 'seeked' event. */
  seekedListener: function (e) {
    this.fireSeekEnd()
  },

  /** Listener for 'ended' event. */
  endedListener: function (e) {
    this.fireStop()
    this.lastPlayhead = 0
  },

  timeupdateListener: function () {
    if (!this.flags.isStarted) {
      this.fireStart()
      if (!this.plugin.getAdsAdapter().flags.isStarted) {
        this.fireJoin()
      }
    }
  }
}, 
{
  GenericAdsAdapter: require('./ads/native')
})

module.exports = youbora.adapters.Dailymotion
