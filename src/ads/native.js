var youbora = require('youboralib')
var manifest = require('../../manifest.json')

var GenericAdsAdapter = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  getPlayhead: function () {
    return this.player.currentTime > this.player.duration ? null : this.player.currentTime
  },

  getResource: function () {
    return this.player.src
  },

  getDuration: function () {
    return this.player.duration
  },

  getPlayerName: function () {
    return 'Dailymotion'
  },

  getPosition: function () {
    return this.plugin.getAdapter().flags.isJoined ? youbora.Constants.AdPosition.Midroll : youbora.Constants.AdPosition.Preroll
  },

  getIsFullscreen: function () {
    return this.player.fullscreen
  },

  getAudioEnabled: function () {
    return !this.player.muted
  },

  getIsVisible: function () {
    return youbora.Util.calculateAdViewability(this.player)
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // References
    this.references = {
      'ad_play': this.playListener.bind(this),
      'ad_pause': this.pauseListener.bind(this),
      'ad_start': this.startListener.bind(this),
      'ad_end': this.endListener.bind(this),
      'ad_timeupdate': this.timeUpdateListener.bind(this),
      'ad_click': this.adClickListener.bind(this),
      'ad_bufferStart': this.bufferingListener.bind(this),
      'ad_bufferFinish': this.bufferedListener.bind(this)
    }
    // Register listeners
    for (var key in this.references) {
      this.player.addEventListener(key, this.references[key])
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.removeEventListener(key, this.references[key])
      }
      delete this.references
    }
  },

  /** Listener for 'ad_play' event. */
  playListener: function (e) {
    if (this.flags.isJoined && !this.flags.isPaused) {
      if (!this.contentplayhead) {
        this.contentplayhead = this.plugin.getAdapter().getPlayhead()
      }
      this.fireStop({ playhead: this.contentplayhead })
      this.fireStart()
      this.fireJoin()
      this.contentplayhead = this.plugin.getAdapter().getPlayhead()
    }
    this.fireJoin()
    this.fireResume()
  },

  /** Listener for 'ad_pause' event. */
  pauseListener: function (e) {
    this.firePause()
  },

  /** Listener for 'ad_start' event. */
  startListener: function (e) {
    this.fireStart()
  },

  /** Listener for 'ad_end' event. */
  endListener: function (e) {
    this.fireStop({ playhead: this.contentplayhead })
  },

  adClickListener: function (e) {
    this.fireClick()
  },

  bufferingListener: function (e) {
    this.fireBufferBegin()
  },

  bufferedListener: function (e) {
    this.fireBufferend()
  },

  /** Listener for 'ad_timeupdate' event. */
  timeUpdateListener: function () {
    var time = this.getPlayhead() / this.getDuration()
    if (time > 0.75) {
      this.fireQuartile(3)
    } else if (time > 0.5) {
      this.fireQuartile(2)
    } else if (time > 0.25) {
      this.fireQuartile(1)
    }
  }
})

module.exports = GenericAdsAdapter
