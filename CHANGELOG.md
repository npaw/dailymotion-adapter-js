## [6.7.0] - 2021-08-09
### Added
- Small refactors
- `waiting`, `ad_click`, `ad_bufferStart` and `ad_bufferFinish` listeners
### Removed
- Playhead monitor
### Library
- Packaged with `lib 6.7.42`

## [6.5.0] - 2019-07-26
### Library
- Packaged with `lib 6.5.8`

## [6.4.0] - 2018-10-01
### Library
- Packaged with `lib 6.4.8`
